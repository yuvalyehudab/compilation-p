# List of Tasks #

Marked approximate difficulty ([H]igh, [M]edium, [L]ow), ordered by priority.

* [M] Use our own `lex` and `cup`.
* [L] Remove all comments, at least all the original ones.
* [L] Remove `canOptimize` everywhere.
* [?] Make sure order of evaluation matches this semester's specification.
* [H] Change implementations in `Manager.java`.
* [H] Remove handling `this`, which we didn't have this semester.
* [L] Replace `for(#TYPE# #index# : #list#) {...}` with `list.forEach(index -> {...})`.
* [M] Auto-format java code.
